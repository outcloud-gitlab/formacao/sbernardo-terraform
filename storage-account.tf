resource "azurerm_resource_group" "this" {
  name     = "rg-${var.student_name}-${var.environment}"
  location = var.location

  tags = local.common_tags
}

resource "azurerm_storage_account" "this" {
  name                            = "outcloud${format("%.13s", replace(var.student_name, "-", ""))}${var.environment}"
  resource_group_name             = azurerm_resource_group.this.name
  location                        = azurerm_resource_group.this.location
  account_tier                    = "Standard"
  account_replication_type        = "LRS"
  allow_nested_items_to_be_public = true

  static_website {
    index_document     = "index.html"
    error_404_document = "index.html"
  }

  tags = local.common_tags
}
